import os
import datetime as datetime

import csv
from csv import writer

from shutil import copyfile

import tkinter as tk
from tkinter import filedialog, messagebox

from filepath_interface import FilepathInterface

from enums import Countries
from timestamp_preformat import TimestampPreformat

class PendingRmas(FilepathInterface):

    def __init__(self, baseFilename, rawFilepath, processedFilepath):

        # initialize interface class
        super().__init__(baseFilename, rawFilepath, processedFilepath)

        # initialize member variables
        titleCan = "Select the Pending RMAs file for Canada"
        titleUsa = "Select the Pending RMAs file for the USA"
        self.filePickTitleArr_ = [titleCan, titleUsa]

        self.inputDateFormat_ = '%Y %m %d'
        self.outputDateFormat_ = '%m/%d/%Y'

        self.initError_ = 0
        # Ask for filepaths
        inputFilepathCan = tk.filedialog.askopenfilename(title = self.filePickTitleArr_[Countries.CANADA.value])
        if not inputFilepathCan:
            self.initError_ = 1

        inputFilepathUsa = tk.filedialog.askopenfilename(title = self.filePickTitleArr_[Countries.USA.value])
        if not inputFilepathUsa:
            self.initError_ = 1

        self.inputFilepathArr_ = [str(inputFilepathCan), str(inputFilepathUsa)]

        # number of lines to delete from the header
        self.headerBlock_ = 6

        completeFilenameCan = self.baseFilename_ + 'CAN' + '-' + 'Processed' + '.csv'
        completeFilenameUsa = self.baseFilename_ + 'USA' + '-' + 'Processed' + '.csv'

        self.completeFilenameArr_ = [completeFilenameCan, completeFilenameUsa]

    def consolidate(self):

        readerOpenDateColumn = 1
        readerCloseDateColumn = 2
        
        timestampPreformat = TimestampPreformat(self.inputDateFormat_,self.outputDateFormat_)


        #copy raw files
        if (self.inputFilepathArr_[Countries.CANADA.value] != os.path.join(self.rawFilepath_, self.completeFilenameArr_[Countries.CANADA.value])):
            copyfile(self.inputFilepathArr_[Countries.CANADA.value], os.path.join(self.rawFilepath_, self.completeFilenameArr_[Countries.CANADA.value]))
        if (self.inputFilepathArr_[Countries.USA.value] != os.path.join(self.rawFilepath_, self.completeFilenameArr_[Countries.USA.value])):
            copyfile(self.inputFilepathArr_[Countries.USA.value], os.path.join(self.rawFilepath_, self.completeFilenameArr_[Countries.USA.value]))
    
        processedFilepathArr = [str(os.path.join(self.processedFilepath_,self.completeFilenameArr_[Countries.CANADA.value])),\
            str(os.path.join(self.processedFilepath_,self.completeFilenameArr_[Countries.USA.value]))]

        for country in range(Countries.SIZE.value):
            print("Processing: ", self.inputFilepathArr_[country])
            with open(self.inputFilepathArr_[country],'rt', encoding="UTF-8") as inputFile:
                #find last line
                reader = csv.reader(inputFile)
                rows = list(reader)
                totalRows = len(rows)
                print("total rows: ", totalRows)
            inputFile.close()

            with open(self.inputFilepathArr_[country],'rt', encoding="UTF-8") as inputFile,\
                open(processedFilepathArr[country],'wt', encoding="UTF-8", newline='') as outputFile:
                reader = csv.reader(inputFile)
                writer = csv.writer(outputFile)

                #set starting line number
                lineNumber = 1
                for row in reader:

                    # Delete the first 6 rows which includes the file header
                    if((lineNumber > self.headerBlock_)):

                        #if not first new row with column names
                        if((lineNumber > (self.headerBlock_+1))):
                            timestampOpen = row[readerOpenDateColumn]
                            timestampClose = row[readerCloseDateColumn]

                            #print("Old timestampOpen:", timestampOpen[:(timestamp.find(":")-2)])

                            timestampOpenReturn = timestampPreformat.preformat(timestampOpen,False)
                            timestampCloseReturn = timestampPreformat.preformat(timestampClose,False)

                            row[readerOpenDateColumn] = timestampOpenReturn
                            row[readerCloseDateColumn] = timestampCloseReturn

                        writer.writerow(row)

                    lineNumber += 1  


            inputFile.close()
            outputFile.close()