import time
from datetime import datetime

class TimestampPreformat:
    def __init__(self,inputDateFormat,outputDateFormat):
        self.inputDateFormat_ = inputDateFormat
        self.outputDateFormat_ = outputDateFormat


    def preformat(self,timestamp, timeInclude):

        #strptime cannot handle "-" characters
        timestamp = timestamp.replace('-',' ')

        if (timeInclude == True):
            
            #pads hours
            timestamp = self.hourPadZero(timestamp)


            timestampFull = self.twentyFourHour(timestamp)

        else:
            timestampFull = timestamp

        #print("Preprocess timestamp:", timestampFull)

        #if not empty timestamp
        if (timestampFull != ''):
            timestampFull = datetime.strptime(timestampFull, self.inputDateFormat_)

            timestampFullReturn = str(timestampFull.strftime(self.outputDateFormat_))
        else:
            timestampFullReturn = str('')

        
        #print("New timestamp:", timestampFullReturn)
        #print("\n\n")

        return(timestampFullReturn)

    #should only be run after hourZeroPad
    def twentyFourHour(self, timestamp):



        # if PM
        if (timestamp[-2] == 'p'):

            #print("timestamp: ", timestamp)

            #remove last 3 characters from the string | removes ' pm' or ' am'
            timestampTwentyFour = timestamp[:(len(timestamp)-3)]

            #print("timestampTwentyFour: ", timestampTwentyFour)


            timeSeparatorIndex = timestamp.find(":")
            paddedHourValue = int(timestamp[(timeSeparatorIndex-2):(timeSeparatorIndex)])
            #print(timestamp[(timeSeparatorIndex-2):(timeSeparatorIndex)])
            #print("Hour value: ", paddedHourValue)

            

            paddedHourValue += 12
            if (paddedHourValue == 24):
                paddedHourValue = str('00')
            else:
                paddedHourValue = str(paddedHourValue)

            #print("24 Hour value: ", paddedHourValue)

            timestampTwentyFourReturn = timestampTwentyFour[ : (timeSeparatorIndex-2)] + paddedHourValue + timestampTwentyFour[(timeSeparatorIndex) : ]

            #print("new timestampTwentyFour", timestampTwentyFourReturn)

            #time.sleep(10)

        # if AM
        elif (timestamp[-2] == 'a'):
            #print("timestamp: ", timestamp)

            #remove last 3 characters from the string | removes ' pm' or ' am'
            timestampTwentyFourReturn = timestamp[:(len(timestamp)-3)]

        # Make sure no trailing empty characters
        else:
            for i in range(2):
                if (timestamp[(-1)] == ' ') :
                    timestamp[(-1)].replace(' ','')

                timestampTwentyFourReturn = timestamp


        return(timestampTwentyFourReturn)

            

    def hourPadZero(self, timestamp):
            
        timeSeparatorIndex = timestamp.find(":")    

        # The hour has no leading 0 and needs one, so:
        # find if two characters before the time separating colon is a space. If it is, add a 0
        hourLeadingZero = timeSeparatorIndex - 2

        if(timestamp[hourLeadingZero] == " "):
            timestampFull  = timestamp[:hourLeadingZero+1] + '0' + timestamp[hourLeadingZero+1:]
        else:
            timestampFull = timestamp

        return(timestampFull)



