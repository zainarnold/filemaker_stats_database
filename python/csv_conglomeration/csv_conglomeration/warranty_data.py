import time
import datetime

import os

import csv
from csv import writer

from shutil import copyfile

import tkinter as tk
from tkinter import filedialog, messagebox

from filepath_interface import FilepathInterface
from timestamp_preformat import TimestampPreformat

#class YearInput(tk.Frame):
#    def __init__(self, master):
#        super().__init__(master)
#        self.pack()

#        self.yearEntry_ = tk.Entry()
#        self.yearEntry_.pack()

#        self.message_ = tk.StringVar()
#        self.message_.set("Set the Year:")

#        self.yearEntry_["textvariable"] = self.message_

#        self.yearEntry_.bind('<Key-Returns>', self.PrintContents)

#    def PrintContents(self, event):
#        print("Content :", self.yearEntry_.get())



# Warranty Data File Parsing & Concatenation
# --------------------------------------------------
class WarrantyData(FilepathInterface): 
    def __init__(self, baseFilename, rawFilepath, processedFilepath, yearVarFile):
        # initialize interface class
        super().__init__(baseFilename, rawFilepath, processedFilepath)
        
        # find years to calculate for
        yearfile = open(yearVarFile, 'r')
        yearVar = 0
        try:
            yearVar = int(yearfile.readline())
        except:
            print("\n")
            print("Normal Warranty Data Collection")
            print("\n")

        if((yearVar != 0) and (yearVar > 1900) and (yearVar <2300)):
            year = yearVar
        else:
            currentTime = datetime.datetime.now()

            print("current Time: ", currentTime)
            year = currentTime.year
            currentWeek = datetime.date(currentTime.year, currentTime.month, currentTime.day).isocalendar()[1]
            print("current Week: ", currentWeek)
            print("\n")


        self.yearsArr_ = [year,year-1, year-2]

        print("The following years will be processed: ")
        for years in self.yearsArr_:
            print(" - ",years)

        print("\n")


        #messageboxMessage = "The current year is: " + str(currentYear) + "\n"\
        #    + "The following years of data will be consolidated:\n"\
        #    + "  - " + str(currentYear) + "\n"\
        #    + "  - " + str(currentYear-1) + "\n"\
        #    + "  - " + str(currentYear-2) + "\n"\
        #    + "\n"\
        #    + "Change your starting year?"
        #changeYear = tk.messagebox.askyesno(title = "Current Year Selection", message = messageboxMessage)

        #time.sleep(5)

        title0 = "Select the warranty data file for " + str(self.yearsArr_[0])
        title1 = "Select the warranty data file for " + str(self.yearsArr_[1])
        title2 = "Select the warranty data file for " + str(self.yearsArr_[2])

        self.filePickTitleArr_ = [title0, title1, title2]
        
        self.inputDateFormat_ = '%Y %m %d'
        self.outputDateFormat_ = '%m/%d/%Y'

        self.initError_ = 0

        inputFilepath0 = tk.filedialog.askopenfilename(title = self.filePickTitleArr_[0])
        if not inputFilepath0:
            self.initError_ = 1
        
        inputFilepath1 = tk.filedialog.askopenfilename(title = self.filePickTitleArr_[1])
        if not inputFilepath1:
            self.initError_ = 1
    
        inputFilepath2 = tk.filedialog.askopenfilename(title = self.filePickTitleArr_[2])
        if not inputFilepath2:
            self.initError_ = 1

        # Put input filepaths into an array
        self.inputFilepathArr_ = [str(inputFilepath0), str(inputFilepath1), str(inputFilepath2)]

        # number of lines to delete from the header
        self.headerBlock_ = 6

        # open input and output files
        completeFilename0 = self.baseFilename_ + str(self.yearsArr_[0]) + '-' + 'Processed' + '.csv'
        completeFilename1 = self.baseFilename_ + str(self.yearsArr_[1]) + '-' + 'Processed' + '.csv'
        completeFilename2 = self.baseFilename_ + str(self.yearsArr_[2]) + '-' + 'Processed' + '.csv'

        self.completeFilenameArr_ = [completeFilename0, completeFilename1, completeFilename2]

    def consolidate(self):

        #variables
        readerDateColumn = 3

        timestampPreformat = TimestampPreformat(self.inputDateFormat_,self.outputDateFormat_)

        #copy raw files
        if (self.inputFilepathArr_[0] != os.path.join(self.rawFilepath_, self.completeFilenameArr_[0])):
            copyfile(self.inputFilepathArr_[0], os.path.join(self.rawFilepath_, self.completeFilenameArr_[0]))

        if (self.inputFilepathArr_[1] != os.path.join(self.rawFilepath_, self.completeFilenameArr_[1])):
            copyfile(self.inputFilepathArr_[1], os.path.join(self.rawFilepath_, self.completeFilenameArr_[1]))

        if (self.inputFilepathArr_[2] != os.path.join(self.rawFilepath_, self.completeFilenameArr_[2])):
            copyfile(self.inputFilepathArr_[2], os.path.join(self.rawFilepath_, self.completeFilenameArr_[2]))

        # Put processed filepaths into an array
        processedFilepathArr = [str(os.path.join(self.processedFilepath_,self.completeFilenameArr_[0])),\
            str(os.path.join(self.processedFilepath_,self.completeFilenameArr_[1])),\
            str(os.path.join(self.processedFilepath_,self.completeFilenameArr_[2]))]

        for fileNumber in range(3):

            print("Processing: ", str(self.inputFilepathArr_[fileNumber]))
            with open(self.inputFilepathArr_[fileNumber],'rt', encoding="UTF-8") as inputFile:
                #find last line
                reader = csv.reader(inputFile)
                rows = list(reader)
                totalRows = len(rows)
                print("total rows: ", totalRows)
            inputFile.close()

            with open(self.inputFilepathArr_[fileNumber],'rt', encoding="UTF-8") as inputFile,\
                open(processedFilepathArr[fileNumber],'wt', encoding="UTF-8", newline='') as outputFile:
                reader = csv.reader(inputFile)
                writer = csv.writer(outputFile)

                #set starting line number
                lineNumber = 1
                for row in reader:

                    # if the first file, include the column headers
                    if fileNumber == 0:
                        # Delete the first 6 rows which includes the file header
                        # Delete the last row which includes the total
                        if((lineNumber > self.headerBlock_) and (lineNumber != totalRows)):
                            writer.writerow(row)
                        lineNumber += 1
                    else:
                        # Delete the first 7 rows which includes the file header and Column headers
                        # Delete the last row which includes the total
                        if((lineNumber > (self.headerBlock_+1)) and (lineNumber != totalRows)):
                            writer.writerow(row)
                        lineNumber += 1  


            inputFile.close()
            outputFile.close()

        # concatenate files
        # --------------------

        # list of input files to be concatenated
        # NOTE: the concatenation starts from the oldest file and works it's way to newer files just so everything is in order 

        lineNumber = 1

        # Open the combined output file
        with open(os.path.join(self.processedFilepath_,"WarrantyData-Noleina-Combined.csv"),'wt',encoding="UTF-8", newline='') as outputCombined:
            writer = csv.writer(outputCombined)

    
            for filename in processedFilepathArr:
                print("Consolidating: ", filename)

                # Open and process file
                with open(filename,'rt', encoding="UTF-8") as input:
                    reader = csv.reader(input)
                    for row in reader:

                        #if not the first row with column names
                        if (lineNumber > 1):
                            #format date as string
                            readerDate = row[readerDateColumn]

                            writerDate = timestampPreformat.preformat(readerDate, False)

                            row[readerDateColumn] = writerDate

                        writer.writerow(row)
                        lineNumber += 1

        print("LineNumberCombined: ", lineNumber)


        return(0)



    
