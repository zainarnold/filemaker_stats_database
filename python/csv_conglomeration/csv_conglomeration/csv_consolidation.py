# Python 3.7 script | excel_conglomeration
# A program to take excel sheet inputs, trim excess empty cells, and output one complete file

# imports
# -------------------------------------------------- 

import os
import shutil
import glob
import time
import datetime
import sys

import tkinter as tk
from tkinter import filedialog, messagebox

from warranty_data import WarrantyData
from pending_rmas import PendingRmas
from rma_close_date_results import RmaCloseDateResults


# variables
# -------------------------------------------------- 
titleFilemakerFile = "Select a Filemaker File"
messageFilemakerFile = "Select the stats database filemaker file"


currentFile = os.path.abspath(__file__)

currentDir = os.path.dirname(currentFile)

foundFilemakerFile = 0

# go up 4 levels, to try to find the filemaker file (fmp12 file)
for dirs in range(4):
    for filename in os.listdir(currentDir):
        if filename.endswith(".fmp12"):
            filemakerFilepath = os.path.join(currentDir,filename)
            foundFilemakerFile = 1
            break
    if(foundFilemakerFile != 1):
        currentDir = os.path.dirname(currentDir)

if((foundFilemakerFile != 1) or (not filemakerFilepath)):
    filemakerFilepath = tk.filedialog.askopenfilename(title = titleFilemakerFile)




dirname =  os.path.dirname(filemakerFilepath)

rawFilepath = os.path.join(dirname,'input\\raw\\')
#print("inputFilepath: ", inputFilepath)
processedFilepath = os.path.join(dirname,'input\\processed\\')

outputFilepath = os.path.join(dirname,'output\\')

successFile = os.path.join(dirname,'var','csv_conglomeration_success.txt')

yearVarFile = os.path.join(dirname,'var','warranty_year_input.txt')
versionVarFile = os.path.join(dirname,'version.txt')

versionFile = open(versionVarFile,'r')
versionVar = str(versionFile.readline())


os.system('cls' if os.name == 'nt' else 'clear')
print("--------------------------------------------------")
print("Filemaker Import File Consolidator")
print(versionVar)
print("\n")
print("Currently works with the following files:")
print(" - PendingRMAs CAD")
print(" - PendingRMAs USA")
print(" - RMACloseDateResults")
print(" - WarrantyData")
print("\n")
print("To run a custom set of 3 years, input the most recent year into: ")
print(" - warranty_year_input.txt | ", yearVarFile )
print("\n")
print("For normal operation, keep warranty_year_input.txt blank")
print("--------------------------------------------------")
print("\n")
time.sleep(2)


#baseFilepath = tk.filedialog.askopenfilename(title = "Select an input Data File")

# Delete Files in input\raw
print("Deleted Files and Directories in: ", rawFilepath)

for filename in os.listdir(rawFilepath):
    file_path = os.path.join(rawFilepath, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
            print("  deleted file: \t", filename)

        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
            print("  deleted dir: \t", file_path)

    except Exception as e:
        print('  Failed to delete %s. Reason: %s' % (file_path, e))

time.sleep(1)

# Delete Files in input\processed
print("Deleted Files and Directories in: ", processedFilepath)

for filename in os.listdir(processedFilepath):
    file_path = os.path.join(processedFilepath, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
            print("  deleted file: \t", filename)

        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
            print("  deleted dir: \t", file_path)

    except Exception as e:
        print('  Failed to delete %s. Reason: %s' % (file_path, e))

time.sleep(1)

# Delete Files in output
print("Deleted Files and Directories in: ", outputFilepath)

for filename in os.listdir(outputFilepath):
    file_path = os.path.join(outputFilepath, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
            print("  deleted file: \t", filename)

        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
            print("  deleted dir: \t", file_path)

        time.sleep(1)

    except Exception as e:
        print('  Failed to delete %s. Reason: %s' % (file_path, e))

time.sleep(4)



baseFilenameWarrantyData = 'WarrantyData-Noleina-'
baseFilenamePendingRmas = 'PendingRMAs-'
baseFilenameRmaCloseDate = 'RMACloseDateResults'


pendingRmas = PendingRmas(baseFilenamePendingRmas, rawFilepath, processedFilepath)
if (pendingRmas.initError_ == 1):
    f = open(successFile, 'w')
    f.write("1")
    f.close()
    exit(1)

rmaCloseDateResults = RmaCloseDateResults(baseFilenameRmaCloseDate, rawFilepath, processedFilepath)
if (rmaCloseDateResults.initError_ == 1):
    f = open(successFile, 'w')
    f.write("1")
    f.close()
    exit(1)

warrantyData = WarrantyData(baseFilenameWarrantyData, rawFilepath, processedFilepath, yearVarFile)
if (warrantyData.initError_ == 1):
    f = open(successFile, 'w')
    f.write("1")
    f.close()
    exit(1)

# main
# -------------------------------------------------- 

error = pendingRmas.consolidate()
if error:
    f = open(successFile, 'w')
    f.write("1")
    f.close()
    exit(1)

error = rmaCloseDateResults.consolidate()
if error:
    f = open(successFile, 'w')
    f.write("1")
    f.close()
    exit(1)

error = warrantyData.consolidate()
if error:
    f = open(successFile, 'w')
    f.write("1")
    f.close()
    exit(1)


print("\n")
time.sleep(5)

# rename raw files in folder
print("Renaming Files in: ", rawFilepath)

for filename in os.listdir(rawFilepath):
    filePath = os.path.join(rawFilepath, filename)
    try:
        if os.path.isfile(filePath) or os.path.islink(filePath):
            #filename, fileExtension = os.path.splitext(filePath)
            newFilename = str(filename)
            newFilename = newFilename.replace("Processed", "Raw")
            newFilepath = os.path.join(rawFilepath, newFilename)
            os.rename(filePath,newFilepath)

        time.sleep(1)

    except Exception as e:
        print('  Failed to rename file %s. Reason: %s' % (filePath, e))

time.sleep(4)

# Processing successful
f = open(successFile, 'w')
f.write("0")
f.close()
exit(0)