import os
import sys
from datetime import datetime
import time

import csv as csv
from csv import reader, writer

from shutil import copyfile

import tkinter as tk
from tkinter import filedialog, messagebox

from filepath_interface import FilepathInterface
from timestamp_preformat import TimestampPreformat

class RmaCloseDateResults(FilepathInterface):
    def __init__(self, baseFilename, rawFilepath, processedFilepath):

        # initialize interface class
        super().__init__(baseFilename, rawFilepath, processedFilepath)

        # initialize member variables
        self.filePickTitle_ = "Select the RMA Close Date Results file"
      
        self.inputDateFormat_ = '%Y %m %d %H:%M'
        self.outputDateFormat_ = '%m/%d/%Y %H:%M'
        
        self.initError_ = 0

        # Ask for Close Date Results file
        self.inputFilepath_ = tk.filedialog.askopenfilename(title = self.filePickTitle_)
        if not self.inputFilepath_:
            self.initError_ = 1


        self.completeFilename_ = self.baseFilename_ + '-' + 'Processed' + '.csv'

    def consolidate(self):

        #variables
        readerOpenDateColumn = 1
        readerCloseDateColumn = 2

        timestampPreformat = TimestampPreformat(self.inputDateFormat_,self.outputDateFormat_)


        #copy raw files
        if not os.path.isfile(os.path.join(self.rawFilepath_, self.completeFilename_)):
            copyfile(self.inputFilepath_, os.path.join(self.rawFilepath_, self.completeFilename_))

        processedFilepathFull = str(os.path.join(self.processedFilepath_, self.completeFilename_))

        print("Processing: ", str(self.inputFilepath_))
        with open(self.inputFilepath_,'rt', encoding="UTF-8") as inputFile:
            #find last line
            reader = csv.reader(inputFile)
            rows = list(reader)
            totalRows = len(rows)
            print("total rows: ", totalRows)
        inputFile.close()

        with open(self.inputFilepath_,'rt', encoding="UTF-8") as inputFile,\
            open(processedFilepathFull, 'wt',encoding="UTF-8", newline='') as outputFile:
            reader = csv.reader(inputFile)
            writer = csv.writer(outputFile)

            #set starting line number
            lineNumber = 1
            for row in reader:

                #if date is formatted incorrectly
                if((lineNumber > 1)):
                    timestampOpen = row[readerOpenDateColumn]
                    timestampClose = row[readerCloseDateColumn]

                    #print("Old timestampOpen:", timestampOpen[:(timestamp.find(":")-2)])

                    timestampOpenReturn = timestampPreformat.preformat(timestampOpen,True)
                    timestampCloseReturn = timestampPreformat.preformat(timestampClose,True)

                    row[readerOpenDateColumn] = timestampOpenReturn
                    row[readerCloseDateColumn] = timestampCloseReturn
                    

                # Delete first column, row[0]
                writer.writerow((row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]))

                lineNumber += 1

        inputFile.close()
        outputFile.close()