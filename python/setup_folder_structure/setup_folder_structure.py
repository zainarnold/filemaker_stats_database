import os
import time

import tkinter as tk
from tkinter import filedialog, messagebox

#application_window = tk.Tk()
#application_window.withdraw()

proceed = tk.messagebox.askokcancel(title="Setup Paths", message="This will set up all folders around the filemaker file that you have selected.\n Proceed?")

print("proceed:", proceed)

if proceed:
    filepath = tk.filedialog.askopenfilename(title = "Select Database")
    print(filepath)
    dirname = os.path.dirname(filepath)
    print(dirname)

    inputFolder = os.path.join(str(dirname),'input')
    print(inputFolder)
	
    outputFolder = os.path.join(str(dirname),'output')
    print(outputFolder)

    rawFolder = os.path.join(str(dirname),'input\\raw\\')
    print(rawFolder)
    
    destFolder = os.path.join(str(dirname),'input\\processed\\')
    print(destFolder)

    varFolder = os.path.join(str(dirname),'var')
    print(varFolder)

    # make input folders
    if not os.path.isdir(inputFolder):
        os.makedirs(inputFolder)

    if not os.path.isdir(rawFolder):
        os.makedirs(rawFolder)

    if not os.path.isdir(destFolder):
        os.makedirs(destFolder)
		
    # make output folders
    if not os.path.isdir(outputFolder):
        os.makedirs(outputFolder)

    #make var folder
    if not os.path.isdir(varFolder):
        os.makedirs(varFolder)

    filemakerLocation = open(os.path.join(str(varFolder),'filemaker_file_location.txt'), 'w')
    filemakerLocation.write(filepath)
    filemakerLocation.close()

time.sleep(1)
